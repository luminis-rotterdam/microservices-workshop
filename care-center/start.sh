#!/bin/sh
if which npm >/dev/null; then
    npm prune
    npm install
    npm start
else
    echo "npm not found, please install node and npm: https://nodejs.org/"
fi
