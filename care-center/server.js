"use strict";
var app = require('express')(),
    config = require('./config.json'),
    consul = require('consul')(config.consul || {}),
    request = require('request'),
    SDC = require('statsd-client'),
    serviceRegistry = {};

bootstrap();

app.get('/api/orders', function (req, res) {
    callService({
        id: 'orders',
        command: function (endpoint) {
            request.get(endpoint + '/orders', function callback(error, response, body) {
                var orders = JSON.parse(body);
                return transformToResponse(orders);
            });
        },
        development: function () {
            var orders = require(__dirname + "/data/orders.json");
            transformToResponse(orders);
        }
    });

    function transformToResponse(orders) {
        var data = orders.map(function (order) {
            var productInfo = order.orderLines.reduce(function (previous, orderLine, index) {
                if (index > 0) {
                    previous += '<br />';
                }
                return previous + orderLine.name + ': ' + orderLine.quantity;
            }, '');
            var contactInfo = order.contactInformation.name + '<br />' + order.contactInformation.email;
            var state = order.shippingState + '<br />' + order.billingState;

            return [order.orderNumber, productInfo, contactInfo, order.cost, state];
        });
        res.json({
            "data": data
        });
    }
});

app.post('/api/returns', function (req, res) {
    var orderNumber = req.body.orderNumber;

    callService({
        id: 'returns',
        command: function (endpoint) {
            request
                .post({
                    url: endpoint + '/returns',
                    body: {
                        orderNumber: orderNumber
                    },
                    json: true
                })
                .pipe(res);
        },
        development: function () {
            console.log('returning order:', orderNumber);
            res.sendStatus(200);
        }
    });
});

app.get('*', function (req, res) {
    res.sendFile(__dirname + "/public/care-center.html");
});

function callService(args) {
    var serviceId = args.id,
        service = findRandomService(serviceId),
        command = args.command,
        development = args.development,
        fallback = args.fallback;

    if (service && command) {
        console.log("Got service endpoint:", service.serviceEndpoint, 'for service id:', serviceId);
        logServiceCall(serviceId);
        return setImmediate(command.bind(this, service.serviceEndpoint));
    } else if (config.services[serviceId].development && development) {
        console.log("Faking service:", serviceId, " because it is development-mode");
        logServiceCall(serviceId, "fake");
        return setTimeout(development, Math.random() * 3000);
    } else if (fallback) {
        console.warn("No service found for:", serviceId, "falling back");
        logServiceCall(serviceId, "fallback");
        return setImmediate(fallback);
    }
    throw new Error("No service or fallback available for service: " + serviceId + ". Please provide one.");

    function findRandomService(serviceId) {
        var servicesAvailable = serviceRegistry[serviceId];
        if (servicesAvailable) {
            return servicesAvailable[Math.floor(Math.random() * servicesAvailable.length)];
        } else {
            return null;
        }
    }

    function logServiceCall(id, prefix) {
        var service = findRandomService('metrics');
        if (service) {
            var sdc = new SDC({host: service.address, port: service.port});
            console.log('Calling metrics for service:', id);
            if (prefix) {
                prefix = "care-center." + prefix + ".";
            } else {
                prefix = "care-center.";
            }
            sdc.increment(prefix + id);
            sdc.close();
        } else {
            console.log("Cannot find 'metrics' service, nothing logged.")
        }
    }

}

function bootstrap() {
    initServer()
    startServer();
    startListeningForConsulServices();
}

function initServer() {
    var express = require('express'),
        bodyParser = require('body-parser');
    request = request.defaults({timeout: 4000});
    app.use(express.static('public'));
    app.use(bodyParser.urlencoded({extended: true}));
}

function startServer() {
    var port = config.port || 4000;
    console.log('liquid-sunshine care-center listening on port:', port);
    app.listen(port);
}

function startListeningForConsulServices() {
    for (var key in config.services) {
        listenForService(key);
    }
}

function listenForService(serviceId) {
    if (config.services[serviceId].development) {
        console.log("Service", serviceId, "is in development mode.");
    } else {
        console.log("Service", serviceId, "is in production mode. Start listening for services");
        createWatch(serviceId)
    }
}

function createWatch(serviceId) {
    var watch = consul.watch({method: consul.health.service, options: {service: serviceId, passing: true}});

    watch.on('change', function (data) {
        var old = JSON.stringify(serviceRegistry);
        serviceRegistry[serviceId] = [];
        data.map(function (serviceInfo) {
            var address = serviceInfo.Service.Address || serviceInfo.Node.Address;
            var port = serviceInfo.Service.Port || 80;
            var serviceEndpoint = 'http://' + address + ':' + port;
            serviceRegistry[serviceId].push({serviceEndpoint: serviceEndpoint});
        });
        if (old !== JSON.stringify(serviceRegistry)) {
            console.log('Something changed for', serviceId)
            console.log(serviceRegistry);
        }
    });

    watch.on('error', function (err) {
        console.error("Error talking to consul for service: " + serviceId + ", did you start the agent?", err);
        process.exit();
    });
}
