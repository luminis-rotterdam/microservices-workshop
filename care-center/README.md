# Liquid Sunshine Care Center quickstart
A simple webserver for the liquid-sunshine care center

## To run
1. install node.js (including NPM): https://nodejs.org/ (build and tested with version 4.0)
2. run start.sh to build and run
3. serve to http://localhost:4000
