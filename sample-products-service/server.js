"use strict";
var app = require('express')();
var config = require('./config.json');
var consulService = require('consul')(config.consul || {}).agent.service;
var port = config.port || 5000;

app.get('/products', function (req, res) {
    setTimeout(function() {
        res.sendFile(__dirname + "/data/products.json");
    }, Math.random * 5000);
});

app.get('/health', function (req, res) {
    setTimeout(function() {
        console.log('Consul is checking my health, feeling fine.');
        res.sendStatus(200);
    }, config.healthCheckTimeout || 1);
});

app.listen(port, function () {
    console.log('product service listening on port', port);
    var serviceRegistration = {
        name: 'products',
        port: port,
        check: {
            http: 'http://localhost:' + port + '/health',
            interval: '5s',
            timeout: '1s'
        }
    };
    consulService.register(serviceRegistration, function(err) {
        if (err) throw err;
        console.log('registered myself as \'products\' with consul.');
    });
});

function exitHandler(exit) {
    consulService.deregister('products', function(err) {
        console.log('de-registered \'products\' with consul.');
        if (exit) {
            process.exit();
        }
    });
}

process.on('beforeExit', exitHandler.bind(null, false));

process.on('SIGINT', exitHandler.bind(null, true));
