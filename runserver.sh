#!/usr/bin/env bash
consul agent -data-dir=/tmp/consul -server -bootstrap-expect 1 -ui -config-dir ./consul/config -bind 127.0.0.1 # -client 10.240.107.138
