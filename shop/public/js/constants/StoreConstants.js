var keyMirror = require('keymirror');

module.exports = keyMirror({
    CART_LOADED: null,
    CART_ADD: null,
    CART_UPDATE: null,
    CART_REMOVE: null,
    CART_CLEAR: null,
    LOGGING_IN: null,
    LOGGED_IN: null,
    LOGIN_FAILED: null,
    LOGGED_OUT: null,
    ORDER_CREATED: null,
    ORDER_CREATING: null,
    ORDER_FAILED: null,
    ORDERS_LOADING: null,
    ORDERS_LOADED: null,
    GET_PRODUCTS: null,
    PRODUCTS_LOADED: null,
    RECOMMENDATIONS_LOADING: null,
    RECOMMENDATIONS_LOADED: null
});
