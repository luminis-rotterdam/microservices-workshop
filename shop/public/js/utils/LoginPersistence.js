var LoginPersistence = {

    loadUserData: function() {
        var userdata = sessionStorage.getItem("userdata");
        return userdata ? JSON.parse(userdata) : null;
    },

    storeUserData: function(userdata) {
        sessionStorage.setItem("userdata", JSON.stringify(userdata));
    },

    logout: function() {
        sessionStorage.removeItem("userdata");
    }

}


module.exports = LoginPersistence;