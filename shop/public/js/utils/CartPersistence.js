var CartPersistence = {

    loadCart: function() {
        var cartData = localStorage.getItem("cart");
        if (cartData) {
            return JSON.parse(cartData);
        } else {
            return [];
        }
    },

    storeCart: function(cart) {
        localStorage.setItem("cart", JSON.stringify(cart));
    }

}


module.exports = CartPersistence;