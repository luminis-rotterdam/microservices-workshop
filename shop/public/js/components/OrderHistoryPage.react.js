var React = require('react');
var Link = require('react-router').Link;
var accounting = require('accounting');

var LoadingComponent = require('./LoadingComponent.react');
var LoginComponent = require('./LoginComponent.react');
var Suggestions = require('./Suggestions.react');

var OrderActions = require('../actions/OrderActions');
var OrderStore = require('../stores/OrderStore');

function getOrderHistoryState() {
    return {
        loading: OrderStore.isLoadingOrderHistory(),
        orders: OrderStore.getOrderHistory()
    };
}

var OrderHistoryPage = React.createClass({

    componentDidMount: function () {
        OrderStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        OrderStore.removeChangeListener(this._onChange);
    },

    getInitialState: function () {
        OrderActions.loadOrders();
        return {
            loading: true
        };
    },

    _onChange: function () {
        this.setState(getOrderHistoryState());
    },

    render: function () {
        return (
            <div className="page-content">

                <ol className="breadcrumb">
                    <li><Link to="/">Home</Link></li>
                    <li>Order history</li>
                </ol>

                <section className="delivery">
                    <div className="container">
                        <div className="row">
                            <LoginComponent {...this.state} showWhenLoggedIn={LoadingComponent} element={OrderHistory}/>
                        </div>
                    </div>
                </section>

                <Suggestions quantity="4" title="You may also like"/>

            </div>
        );
    }
});

function getShippingStateClassName(state) {
    if (state === 'delivered') {
        return "delivered"
    } else {
        return "in-progress";
    }
}

function getOrderImage(order) {
    return '/img/products/' + order.orderLines[0].category + '/' + order.orderLines[0].name + '.png';
}

function getCollapseClassName(index) {
    if (index === 0) {
        return 'collapse in';
    } else {
        return 'collapse';
    }
}

function getImageClassName(index) {
    if (index === 0) {
        return 'delivery-preview historyImgShow';
    } else {
        return 'delivery-preview';
    }
}

var OrderHistory = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    componentDidUpdate: function () {
        $('.panel-heading').click(function () {
            var hisroryID = $(this).data('img');
            $('.delivery-preview').removeClass('historyImgShow');
            $(hisroryID).addClass('historyImgShow');
        });
    },

    onDelivery: function (orderNumber) {
        this.context.router.push('tracking/' + orderNumber);
    },

    render: function () {
        var orders = this.props.orders;
        return (
            <div className="row">

                <div className="col-lg-5 col-md-5">
                    <div className="delivery-img-container">
                        {
                            orders.map(function (order, index) {
                                return (
                                    <div key={order.orderNumber} className={getImageClassName(index)}
                                         id={'historyImg_' + order.orderNumber}>
                                        <img
                                            src={getOrderImage(order)}/>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>

                <div className="col-lg-7 col-md-7">
                    <div className="accordion panel-group" id="accordion">
                        {
                            orders.map(function (order, index) {
                                return (
                                    <div key={order.orderNumber} className="panel">
                                        <div className="panel-heading active"
                                             data-img={'#historyImg_' + order.orderNumber}>
                                            <a data-toggle="collapse" data-parent="#accordion"
                                               href={'#collapse' + order.orderNumber}>
                                                <div className="badges">
                                                    <span
                                                        className={getShippingStateClassName(order.shippingState)}>{order.shippingState}</span>
                                                </div>

                                                <span className="gray-color date-time">{order.date}</span>

                                                <div className="history-title">{order.description}</div>

                                                <span
                                                    className="history-cost">{accounting.formatMoney(order.cost)}</span>
                                            </a>
                                        </div>
                                        <div id={'collapse' + order.orderNumber}
                                             className={getCollapseClassName(index)}>
                                            <div className="panel-body">
                                                <table className="delivery-details">
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="5" className="orer-heading">Order details</td>
                                                        </tr>
                                                        <tr>
                                                            <td className="details-heading">Product:</td>
                                                            <td className="details-heading">Quantity:</td>
                                                            <td className="details-heading">Shipping:</td>
                                                            <td className="details-heading">Subtotal:</td>
                                                            <td className="details-heading">Total:</td>
                                                        </tr>
                                                        {
                                                            order.orderLines.map(function (line, index) {
                                                                return (
                                                                    <tr key={index}>
                                                                        <td>{line.name}</td>
                                                                        <td>{line.quantity}</td>
                                                                        <td>Free</td>
                                                                        <td>{accounting.formatMoney(line.cost)}</td>
                                                                        <td>{accounting.formatMoney(line.cost)}</td>
                                                                    </tr>
                                                                );
                                                            })
                                                        }
                                                    </tbody>
                                                </table>

                                                <table className="delivery-details">
                                                    <tbody>
                                                        <tr>
                                                            <td colSpan="3" className="orer-heading">Customer details</td>
                                                        </tr>
                                                        <tr>
                                                            <td className="details-heading">Mail/Phone</td>
                                                            <td className="details-heading">Billing address</td>
                                                            <td className="details-heading">Shipping address</td>
                                                        </tr>
                                                        <tr>
                                                            <td>{order.contactInformation.email}</td>
                                                            <td>{order.billingAddress.street}</td>
                                                            <td>{order.shippingAddress.street}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>{order.contactInformation.phone}</td>
                                                            <td>{order.billingAddress.city + ', ' + order.billingAddress.postCode}</td>
                                                            <td>{order.shippingAddress.city + ', ' + order.shippingAddress.postCode}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="panel-footer clearfix">
                                                <button className="btn btn-primary btn-sm pull-right"
                                                        onClick={this.onDelivery.bind(this, order.orderNumber)}>Show
                                                    delivery
                                                    info
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                );
                            }.bind(this))
                        }
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = OrderHistoryPage;