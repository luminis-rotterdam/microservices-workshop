var React = require('react');
var Link = require('react-router').Link;
var accounting = require('accounting');

var Suggestions = require('./Suggestions.react');

var CartActions = require('../actions/CartActions');
var CartStore = require('../stores/CartStore');

var LoginComponent = require('./LoginComponent.react');
var LoginStore = require('../stores/LoginStore');

var OrderActions = require('../actions/OrderActions');
var OrderStore = require('../stores/OrderStore');

function getCheckoutState() {
    return {
        cartItems: CartStore.getAll(),
        cartTotalCost: accounting.formatMoney(CartStore.getTotalCost()),
        cartTotalItems: CartStore.getTotalItems(),
        userInfo: LoginStore.getUserInfo(),
        loginState: LoginStore.getState(),
        orderFailed: OrderStore.isOrderFailed(),
        orderInProgress: OrderStore.isOrderInProgress()
    };
}

var CheckoutPage = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    
    onOrder: function (event) {
        event.preventDefault();
        OrderActions.placeOrder();
    },

    getInitialState: function () {
        return getCheckoutState();
    },

    componentDidMount: function () {
        CartStore.addChangeListener(this._onChange);
        LoginStore.addChangeListener(this._onChange);
        OrderStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        CartStore.removeChangeListener(this._onChange);
        LoginStore.addChangeListener(this._onChange);
        OrderStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        if (OrderStore.hasNewOrder()) {
            return this.context.router.push('tracking/' + OrderStore.getLastOrderNumner());
        }
        this.setState(getCheckoutState());
    },

    render: function () {
        return (
            <div className="page-content">

                <ol className="breadcrumb">
                    <li><Link to="/">Home</Link></li>
                    <li>Checkout</li>
                </ol>

                <section className="checkout">
                    <div className="container">

                        <div className="row">
                            <div className="col-lg-12">
                                <h2>Checkout</h2>
                                {
                                    function () {
                                        if (this.state.orderFailed) {
                                            return (
                                                <div className="alert alert-danger alert-dismissible fade in"
                                                     role="alert">
                                                    <strong>Could not create your order!</strong> Please try again, and
                                                    hope for the best.
                                                </div>
                                            );
                                        }
                                    }.bind(this)()
                                }
                                <div className="row">
                                    <LoginComponent {...this.state} showWhenLoggedIn={CheckoutDetails} />

                                    <div className="col-lg-3 col-lg-offset-1 col-md-4 col-sm-4">
                                        <h3>Your order</h3>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th>Product</th>
                                            </tr>
                                            {
                                                this.state.cartItems.map(function (item) {
                                                    return (
                                                        <tr key={item.id}>
                                                            <td className="name border">{item.name}<span>{'x' + item.amount}</span>
                                                            </td>
                                                            <td className="price border">{accounting.formatMoney(item.cost, {
                                                                symbol: " €",
                                                                format: "%v %s"
                                                            }, 2)}</td>
                                                        </tr>
                                                    );
                                                })
                                            }
                                            <tr>
                                                <td className="th">Cart subtotal</td>
                                                <td className="price">{this.state.cartTotalCost}</td>
                                            </tr>
                                            <tr>
                                                <td className="th border">Shipping</td>
                                                <td className="align-r border">Free shipping</td>
                                            </tr>
                                            <tr>
                                                <td className="th">Order total</td>
                                                <td className="price">{this.state.cartTotalCost}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        {
                                            function () {
                                                if (LoginStore.isLoggedIn()) {
                                                    if (this.state.orderInProgress) {
                                                        return (
                                                            <h2 className="spinner"><i
                                                                className="fa fa-spinner fa-spin"></i></h2>
                                                        );
                                                    } else {
                                                        return (
                                                            <form id="checkout-form" method="post"
                                                                  onSubmit={this.onOrder}>
                                                                <div className="payment-method">
                                                                    <div className="radio light">
                                                                        <label><input type="radio" name="payment"
                                                                                      id="payment01" defaultChecked="true"/>
                                                                            Direct Bank Transfer</label>
                                                                    </div>
                                                                    <p>Make your payment directly into our bank account.
                                                                        Please use your Order
                                                                        ID as the payment reference. Your order will not
                                                                        be shipped until the
                                                                        funds have cleared in our account.</p>

                                                                    <div className="radio light">
                                                                        <label><input type="radio" name="payment"
                                                                                      id="payment02"/> Cheque
                                                                            Payment</label>
                                                                    </div>
                                                                    <div className="radio light">
                                                                        <label><input type="radio" name="payment"
                                                                                      id="payment03"/> PayPal <span
                                                                            className="pp-label"></span></label>
                                                                    </div>
                                                                </div>

                                                                <input className="btn btn-black btn-block" type="submit"
                                                                       name="place-order"
                                                                       value="Place order"/>
                                                            </form>
                                                        );
                                                    }
                                                }
                                            }.bind(this)()
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
});

var CheckoutDetails = React.createClass({
    render: function () {
        var userInfo = this.props.userInfo;
        return (
            <div className="col-lg-8 col-md-8 col-sm-8">

                <h3>Billing address</h3>

                <div className="form-group">
                    <label htmlFor="co-country">Country *</label>

                    <input type="text" className="form-control"
                           id="co-country"
                           readOnly="true"
                           name="co-country"
                           required="true" value={userInfo.country}/>
                </div>
                <div className="row">
                    <div className="form-group col-lg-6 col-md-6 col-sm-6">
                        <label htmlFor="co-first-name">First Name *</label>
                        <input type="text" className="form-control"
                               id="co-first-name"
                               readOnly="true"
                               name="co-first-name" placeholder="First name"
                               required="true"
                               value={userInfo.firstName}/>
                    </div>
                    <div className="form-group col-lg-6 col-md-6 col-sm-6">
                        <label htmlFor="co-last-name">Last Name *</label>
                        <input type="text" className="form-control"
                               id="co-last-name"
                               readOnly="true"
                               name="co-last-name" placeholder="Last name"
                               required="true"
                               value={userInfo.lastName}/>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="co-str-adress">Adress *</label>
                    <input type="text" className="form-control"
                           id="co-str-adress"
                           readOnly="true"
                           name="co-str-adress" placeholder="Street adress"
                           required="true" value={userInfo.street}/>
                </div>
                <div className="form-group">
                    <label htmlFor="co-city">Town/ city *</label>
                    <input type="text" className="form-control" id="co-city"
                           readOnly="true"
                           name="co-city"
                           placeholder="Town/ city" required="true"
                           value={userInfo.city}/>
                </div>
                <div className="row">
                    <div className="form-group col-lg-6 col-md-6 col-sm-6">
                        <label htmlFor="co-state">County/ state</label>
                        <input type="text" className="form-control"
                               id="co-state"
                               readOnly="true"
                               name="co-state" placeholder="County/ state"
                               value={userInfo.state}/>
                    </div>
                    <div className="form-group col-lg-6 col-md-6 col-sm-6">
                        <label htmlFor="co_postcode">Postcode *</label>
                        <input type="text" className="form-control"
                               id="co_postcode"
                               readOnly="true"
                               name="co_postcode"
                               placeholder="Postcode/ ZIP" required="true"
                               value={userInfo.postCode}/>
                    </div>
                </div>
                <div className="row">
                    <div className="form-group col-lg-6 col-md-6 col-sm-6">
                        <label htmlFor="co-email">Email *</label>
                        <input type="email" className="form-control"
                               id="co-email"
                               readOnly="true"
                               name="co-email" placeholder="Email adress"
                               required="true"
                               value={userInfo.email}/>
                    </div>
                    <div className="form-group col-lg-6 col-md-6 col-sm-6">
                        <label htmlFor="co_phone">Phone *</label>
                        <input type="text" className="form-control"
                               id="co_phone"
                               readOnly="true"
                               name="co_phone" placeholder="Phone number"
                               required="true"
                               value={userInfo.phone}/>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = CheckoutPage;