var React = require('react');
var Link = require('react-router').Link;
var ProductStore = require('../stores/ProductStore');
var ProductsActions = require('../actions/ProductActions');

var ProductTile = require('./ProductTile.react');

function getProducts(category) {
    var products = undefined;
    if (category) {
        products = ProductStore.getProductsForCategory(category)
    } else {
        products = ProductStore.getProducts();
    }
    return {
        products: products,
        isLoading: ProductStore.isLoading(),
        development: ProductStore.isDevelopment()
    }
}

function getLabelForCategory(category) {
    switch (category) {
        case 'blends':
            return "Blends";
        case 'single-malt':
            return "Single Malt";
        case 'irish':
            return "Irish";
        case 'international':
            return "International";
        case 'bourbon':
            return "Bourbon";
        case 'exclusive':
            return "Exclusive";
        default:
            return "All Whiskies";
    }
}

function getClassForRow(development) {
    return development ? "row development" : "row";
}

var ProductListPage = React.createClass({

    getInitialState: function () {
        var query = this.props.location.query['search-hd'];
        ProductsActions.loadProducts(query);
        return {isLoading: true};
    },

    componentDidMount: function () {
        ProductStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        ProductStore.removeChangeListener(this._onChange);
    },

    componentWillReceiveProps: function (nextProps) {
        var oldCategory = this.props.params.category;
        var newCategory = nextProps.params.category;
        var oldQuery = this.props.location.query['search-hd'];
        var newQuery = nextProps.location.query['search-hd'];

        if (oldQuery != newQuery) {
            ProductsActions.loadProducts(newQuery);
            this.setState({isLoading: true});
        } else if (oldCategory != newCategory) {
            this.setState(getProducts(newCategory));
        }
    },

    /**
     * Event handler for 'change' events coming from the ProductStore
     */
    _onChange: function () {
        this.setState(getProducts(this.props.params.category));
    },

    render: function () {
        return (
            <div className="page-content">
                <ol className="breadcrumb">
                    <li><Link to="/">Home</Link></li>
                    <li>Shop - {getLabelForCategory(this.props.params.category)}</li>
                </ol>
                <section className="catalog-grid">
                    {
                        function () {
                            if (this.state.isLoading) {
                                return (
                                    <div className="container">
                                        <h2 className="spinner"><i className="fa fa-spinner fa-spin"></i></h2>
                                    </div>
                                )
                            } else {
                                return (
                                    <div className="container">
                                        <h2 className="with-sorting">{getLabelForCategory(this.props.params.category)}</h2>

                                        <div className={getClassForRow(this.state.development)}>
                                            {
                                                this.state.products.map(function (product) {
                                                    return (
                                                        <ProductTile className="col-lg-4 col-md-4 col-sm-6" key={product.id} product={product}/>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                )
                            }
                        }.bind(this)()
                    }
                </section>
            </div>
        );
    }
});

module.exports = ProductListPage;