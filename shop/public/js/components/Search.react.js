var React = require('react');

var Search = React.createClass({

    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    render: function () {
        return (
            <span>
                <form action={this.context.router.createPath('/products')} className="search-form closed" method="get" role="form" autoComplete="off" onSubmit={this.onSearch}>
                    <div className="container">
                        <div className="close-search"><i className="icon-delete"></i></div>
                        <div className="form-group">
                            <label className="sr-only" htmlFor="search-hd">Search for product</label>
                            <input type="text" className="form-control" name="search-hd" id="search-hd"
                                   placeholder="Search for product"/>
                            <button type="submit"><i className="icon-magnifier"></i></button>
                        </div>
                    </div>
                </form>
            </span>
        );
    }
});

module.exports = Search;