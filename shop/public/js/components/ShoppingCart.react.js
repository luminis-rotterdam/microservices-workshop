var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var accounting = require('accounting');

var CartActions = require('../actions/CartActions');
var CartStore = require('../stores/CartStore');
var CartPersistence = require('../utils/CartPersistence');

function getCartState() {
    return {
        cartItems: CartStore.getAll(),
        cartTotalCost: accounting.formatMoney(CartStore.getTotalCost()),
        cartTotalItems: CartStore.getTotalItems()
    };
}

var ShoppingCart = React.createClass({
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },

    onDelete: function (id, event) {
        event.preventDefault();
        CartActions.remove(id);
    },

    onUpdate: function (id, amount, event) {
        event.preventDefault();
        CartActions.update(id, amount);
    },

    onCheckout: function (event) {
        event.preventDefault();
        this.context.router.push('checkout');
    },

    getInitialState: function () {
        return getCartState();
    },

    componentDidMount: function () {
        CartStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        CartStore.removeChangeListener(this._onChange);
    },

    render: function () {
        return (
            <section className="shopping-cart">
                <div className="container">
                    <div className="row">

                        <div className="col-lg-9 col-md-9">
                            <h2 className="title">Shopping cart</h2>
                            <table className="items-list">
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Product name</th>
                                    <th>Product price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.state.cartItems.map(function (item) {
                                        return (
                                            <tr key={item.id} className="item first">
                                                <td className="thumb"><Link
                                                    to={`/products/${item.category}/${item.id}`}>
                                                    <img
                                                        src={`/img/products/${item.category}/${item.name}.png`}
                                                        alt={item.name} width="152" height="119"/></Link>
                                                </td>
                                                <td className="name"><Link
                                                    to={`/products/${item.category}/${item.id}`}>{item.name}</Link>
                                                </td>
                                                <td className="price">{accounting.formatMoney(item.productCost)}</td>
                                                <td className="qnt-count">
                                                    <a className="incr-btn" href="#"
                                                       onClick={this.onUpdate.bind(this, item.id, item.amount -1)}>-</a>
                                                    <input className="quantity form-control" type="number"
                                                           value={item.amount} readOnly="true"/>
                                                    <a className="incr-btn" href="#"
                                                       onClick={this.onUpdate.bind(this, item.id, item.amount + 1)}>+</a>
                                                </td>
                                                <td className="total">{accounting.formatMoney(item.cost)}</td>
                                                <td className="delete"><i className="icon-delete"
                                                                          onClick={this.onDelete.bind(this, item.id)}></i>
                                                </td>
                                            </tr>
                                        )
                                    }.bind(this))
                                }
                                </tbody>
                            </table>
                        </div>

                        <div className="col-lg-3 col-md-3">
                            <h3>Cart totals</h3>

                            <form className="cart-sidebar" method="post">
                                <div className="cart-totals">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>Cart subtotal</td>
                                                <td className="total align-r">{this.state.cartTotalCost + ' $'}</td>
                                            </tr>
                                            <tr className="devider">
                                                <td>Shipping</td>
                                                <td className="align-r">Free shipping</td>
                                            </tr>
                                            <tr>
                                                <td>Order total</td>
                                                <td className="total align-r">{this.state.cartTotalCost + ' $'}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <input type="submit" className="btn btn-black btn-block" name="to-checkout"
                                           value="Proceed to checkout" onClick={this.onCheckout}/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );
    },

    /**
     * Event handler for 'change' events coming from the CartStore
     */
    _onChange: function () {
        CartPersistence.storeCart(CartStore.getAll());
        this.setState(getCartState());
    }

});

module.exports = ShoppingCart;