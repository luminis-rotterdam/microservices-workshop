var React = require('react');

var LoadingComponent = React.createClass({

    propTypes: {
        loading: React.PropTypes.bool.isRequired,
        element: React.PropTypes.any.isRequired,
        className: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {
            className: 'col-lg-8 col-md-8 col-sm-8'
        };
    },

    render: function () {
        if (this.props.loading) {
            return (
                <div className="container">
                    <h2 className="spinner"><i className="fa fa-spinner fa-spin"></i></h2>
                </div>
            );
        } else {
            var Element = this.props.element;
            return (
                <Element {...this.props} />
            )
        }
    }

});

module.exports = LoadingComponent;