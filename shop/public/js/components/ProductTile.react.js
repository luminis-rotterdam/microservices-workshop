var React = require('react');
var Link = require('react-router').Link;
var CartActions = require('../actions/CartActions');
var accounting = require('accounting');

var ProductTile = React.createClass({

    handleClick: function () {
        CartActions.add(this.props.product);
    },

    render: function () {
        return (
            <div className={this.props.className}>
                <div className="tile">
                    <div className="badges">
                        <span className="sale">Sale</span>
                    </div>
                    <div className="price-label">{accounting.formatMoney(this.props.product.cost)}</div>
                    <Link to={`/products/${this.props.product.category}/${this.props.product.id}`}>
                        <img
                            src={'/img/products/' + this.props.product.category + '/' + this.props.product.name + '.png'}
                            alt={this.props.product.name}/>
                        <span className="tile-overlay"></span>
                    </Link>

                    <div className="footer">
                        <a href="#">{this.props.product.brand}</a>
                        <span>{this.props.product.name}</span>
                        <button onClick={this.handleClick} className="btn btn-primary">Add to Cart</button>
                    </div>
                </div>
            </div>
        );
    }

});

module.exports = ProductTile;