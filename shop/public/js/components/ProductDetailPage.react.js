var React = require('react');
var Link = require('react-router').Link;
var ProductStore = require('../stores/ProductStore');
var ProductsActions = require('../actions/ProductActions');
var CartActions = require('../actions/CartActions');

var accounting = require('accounting');

function getProduct(id) {
    return {
        product: ProductStore.findBy(id),
        isLoading: ProductStore.isLoading()
    }
}

function getLabelForCategory(category) {
    switch (category) {
        case 'blends':
            return "Blends";
        case 'single-malt':
            return "Single Malt";
        case 'irish':
            return "Irish";
        case 'international':
            return "International";
        case 'bourbon':
            return "Bourbon";
        case 'exclusive':
            return "Exclusive";
        default:
            return "All Whiskies";
    }
}

var ProductDetailPage = React.createClass({

    getInitialState: function () {
        ProductsActions.loadProducts();
        return {
            product: {},
            isLoading: true
        };
    },

    componentDidMount: function () {
        ProductStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        ProductStore.removeChangeListener(this._onChange);
    },

    componentWillReceiveProps: function (nextProps) {
        this.setState(getProduct(nextProps.params.id));
    },

    componentDidUpdate: function() {
        if($('#prod-gal').length > 0) {
            var categorySlider = new MasterSlider();
            categorySlider.control('thumblist' , {autohide:false ,dir:'h',align:'bottom', width:137, height:130, margin:15, space:0 , hideUnder:400});
            categorySlider.setup('prod-gal' , {
                width:550,
                height:484,
                speed: 25,
                preload:'all',
                loop:true,
                view:'fade'
            });
        }
    },

    _onChange: function () {
        this.setState(getProduct(this.props.params.id));
    },

    handleClick: function () {
        CartActions.add(this.state.product);
    },

    render: function () {
        return (
            <div className="page-content">

                <ol className="breadcrumb">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to={`/products/${this.props.params.category}`}>Shop
                        - {getLabelForCategory(this.props.params.category)}</Link></li>
                    <li>{this.state.product.name}</li>
                </ol>

                <section className="catalog-single">
                    {
                        function () {
                            if (this.state.isLoading) {
                                return (
                                    <div className="container">
                                        <h2 className="spinner"><i className="fa fa-spinner fa-spin"></i></h2>
                                    </div>
                                )
                            } else {
                                return (
                                    <div className="container">
                                        <div className="row">

                                            <div className="col-lg-6 col-md-6">
                                                <div className="prod-gal master-slider" id="prod-gal">
                                                    <div className="ms-slide">
                                                        <img src="masterslider/blank.gif"
                                                             data-src={'/img/products/' + this.state.product.category + '/' + this.state.product.name + '.png'}
                                                             alt={this.state.product.name}/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="col-lg-6 col-md-6">
                                                <h1>{this.state.product.name}</h1>

                                                <div className="price">{accounting.formatMoney(this.state.product.cost)}</div>
                                                <div className="buttons group">
                                                    <a onClick={this.handleClick} className="btn btn-primary btn-sm" id="addItemToCart" href="#"><i
                                                        className="icon-shopping-cart"></i>Add to cart</a>
                                                </div>
                                                <p className="p-style2">{this.state.product.description}</p>

                                                <div className="row">
                                                    <div className="col-lg-4 col-md-4 col-sm-5">
                                                        <h3>Tell friends</h3>

                                                        <div className="social-links">
                                                            <a href="#"><i className="fa fa-tumblr-square"></i></a>
                                                            <a href="#"><i className="fa fa-pinterest-square"></i></a>
                                                            <a href="#"><i className="fa fa-facebook-square"></i></a>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-8 col-md-8 col-sm-7">
                                                        <h3>Tags</h3>

                                                        <div className="tags">
                                                            <a href="#">{this.state.product.brand}</a>,
                                                            <a href="#">{this.state.product.category}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="promo-labels">
                                                    <div
                                                        data-content="This is a place for the unique commercial offer. Make it known.">
                                                        <span><i className="fa fa-truck"></i>Free delivery</span></div>
                                                    <div
                                                        data-content="This is a place for the unique commercial offer. Make it known.">
                                                        <i className="fa fa-space-shuttle"></i>Deliver even on Mars
                                                    </div>
                                                    <div
                                                        data-content="This is a place for the unique commercial offer. Make it known.">
                                                        <i className="fa fa-shield"></i>Safe Buy
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                )
                            }
                        }.bind(this)()
                    }
                </section>

            </div>
        );
    }
});

module.exports = ProductDetailPage;