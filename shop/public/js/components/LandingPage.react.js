var React = require('react');
var Link = require('react-router').Link;

var Suggestions = require('./Suggestions.react');

var LandingPage = React.createClass({
    render: function () {
        return (
            <div className="page-content">

                <section className="hero-slider">
                    <div className="master-slider" id="hero-slider">

                        <div className="ms-slide" data-delay="7">
                            <div className="overlay"></div>
                            <img src="masterslider/blank.gif" data-src="img/hero/slideshow/whisky-hq.jpg" alt=""/>

                            <h2 style={{width: "456px", left: "110px", top: "110px"}} className="dark-color ms-layer"
                                data-effect="top(50,true)" data-duration="700" data-delay="250" data-ease="easeOutQuad">
                                Whisky is like Liquid Sunshine!</h2>

                            <p style={{width: "456px", left: "110px", top: "210px"}} className="dark-color ms-layer"
                               data-effect="back(500)"
                               data-duration="700" data-delay="500" data-ease="easeOutQuad">We welcome more than 27.5 million customers every year across our growing estate of 22 shops in Europe.</p>

                            <div style={{left: "110px", top: "300px"}} className="ms-layer button" data-effect="left(50,true)"
                                 data-duration="500" data-delay="750" data-ease="easeOutQuad"><Link className="btn btn-black"
                                                                                                 to="/products">Go
                                to catalog</Link></div>
                        </div>

                    </div>
                </section>

                <section className="cat-tiles">
                    <div className="container">
                        <h2>Browse categories</h2>

                        <div className="row">
                            <div className="category col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <Link to="/products/blends">
                                    <img src="img/categories/blend.png" alt="1"/>
                                    <p>Blends</p>
                                </Link>
                            </div>
                            <div className="category col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <Link to="/products/single-malt">
                                    <img src="img/categories/malt.png" alt="1"/>
                                    <p>Single Malt</p>
                                </Link>
                            </div>
                            <div className="category col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <Link to="/products/irish">
                                    <img src="img/categories/irish.png" alt="1"/>
                                    <p>Irish</p>
                                </Link>
                            </div>
                            <div className="category col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <Link to="/products/bourbon">
                                    <img src="img/categories/bourbon.gif" alt="1"/>
                                    <p>Bourbon</p>
                                </Link>
                            </div>
                            <div className="category col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <Link to="/products/international">
                                    <img src="img/categories/international.png" alt="1"/>
                                    <p>International</p>
                                </Link>
                            </div>
                            <div className="category col-lg-2 col-md-2 col-sm-4 col-xs-6">
                                <Link to="/products/exclusive">
                                    <img src="img/categories/exclusive.png" alt="1"/>
                                    <p>Exclusive</p>
                                </Link>
                            </div>
                        </div>
                    </div>
                </section>

                <Suggestions quantity="4" title="Catalog picks"/>
            </div>
        );
    }
});

module.exports = LandingPage;