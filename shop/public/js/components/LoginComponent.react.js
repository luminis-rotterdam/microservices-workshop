var React = require('react');

var LoginActions = require('../actions/LoginActions');
var LoginStore = require('../stores/LoginStore');
var LoginConstants = require('../constants/StoreConstants');

function getLoginState() {
    return {
        loginState: LoginStore.getState(),
    };
}

var LoginComponent = React.createClass({

    propTypes: {
        showWhenLoggedIn: React.PropTypes.any.isRequired
    },

    onLogin: function (event) {
        event.preventDefault();
        LoginActions.login(document.getElementById("log-email2").value, document.getElementById("log-password2").value);
    },

    getInitialState: function () {
        return getLoginState();
    },

    componentDidMount: function () {
        LoginStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        LoginStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState(getLoginState());
    },

    render: function () {
        switch (this.state.loginState) {
            case LoginConstants.LOGGING_IN:
                return (
                    <div className="col-lg-8 col-md-8 col-sm-8">
                        <h2 className="spinner"><i className="fa fa-spinner fa-spin"></i></h2>
                    </div>
                );
            case LoginConstants.LOGIN_FAILED:
            case LoginConstants.LOGGED_OUT:
                return (
                    <div className="col-lg-8 col-md-8 col-sm-8">
                        <h3>Login</h3>
                        {
                            function () {
                                if (this.state.loginState == LoginConstants.LOGIN_FAILED) {
                                    return (
                                        <div className="alert alert-warning alert-dismissible fade in"
                                             role="alert">
                                            <strong>Could not log you in!</strong> Please try again, and
                                            hope for the best.
                                        </div>
                                    );
                                }
                            }.bind(this)()
                        }
                        <form className="login-form" method="post" onSubmit={this.onLogin}>
                            <div className="form-group group">
                                <label htmlFor="log-email2">Email</label>
                                <input type="email" className="form-control"
                                       name="log-email2" id="log-email2"
                                       placeholder="Enter your email" required/>
                            </div>
                            <div className="form-group group">
                                <label htmlFor="log-password2">Password</label>
                                <input type="password" className="form-control"
                                       name="log-password2" id="log-password2"
                                       placeholder="Enter your password" required/>
                            </div>
                            <input className="btn btn-primary" type="submit"
                                   value="Login"/>
                        </form>
                    </div>
                );
            case LoginConstants.LOGGED_IN:
                var Element = this.props.showWhenLoggedIn;
                return (
                    <Element {...this.props} />
                );
        }
    }
});

module.exports = LoginComponent;
