var React = require('react');

var Footer = React.createClass({
    render: function() {
        return (
            <footer className="footer">
                <div className="sticky-btns">
                    <span id="scrollTop-btn"><i className="fa fa-chevron-up"></i></span>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-5 col-md-5 col-sm-5">
                            <div className="info">
                                <a className="logo" href="index.html"><img src="/custom-images/new-logo-footer.png" alt="Liquid Sunshine"/></a>
                                <p>Too much of anything is bad, but too much good whiskey is barely enough.</p>
                                <div className="social">
                                    <a href="https://twitter.com/LuminisRdam" target="_blank"><i className="fa fa-twitter-square"></i></a>
                                    <a href="https://plus.google.com/s/Luminis" target="_blank"><i className="fa fa-google-plus-square"></i></a>
                                    <a href="https://www.linkedin.com/company/luminis" target="_blank"><i className="fa fa-linkedin-square"></i></a>
                                    <a href="https://www.youtube.com/channel/UC6ALyxOTtVmU9qghBfZ3KaA" target="_blank"><i className="fa fa-youtube-square"></i></a>
                                    <a href="https://vimeo.com/user17212182" target="_blank"><i className="fa fa-vimeo-square"></i></a>
                                    <a href="https://www.facebook.com/pages/Luminis-Rotterdam/252538168109335" target="_blank"><i className="fa fa-facebook-square"></i></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-4 col-sm-4">
                            <h2>Latest news</h2>
                            <ul className="list-unstyled">
                                <li>25 of May <a href="https://google.com/search?q=new+whisky+brands" target="_blank">new arrivals</a></li>
                                <li>24 of April <a href="https://google.com/search?q=facts+about+whisky" target="_blank">facts about whisky</a></li>
                                <li>25 of March <a href="https://google.com/search?q=spring+whisky" target="_blank">spring whisky's</a></li>
                                <li>23 of Februari <a href="https://google.com/search?q=whisky+keep+warm" target="_blank">keep warm in winter</a></li>
                            </ul>
                        </div>
                        <div className="contacts col-lg-3 col-md-3 col-sm-3">
                            <h2>Contacts</h2>
                            <p className="p-style3">
                                Kasteelweg 51,<br/>
                                3077 BN  Rotterdam<br/>
                                <a href="mailto:info.rotterdam@luminis.eu">info.rotterdam@luminis.eu</a><br/>
                                +31 88 58 64 640<br/>
                            </p>
                        </div>
                    </div>
                    <div className="copyright">
                        <div className="row">
                            <div className="col-lg-7 col-md-7 col-sm-7">
                                <p>&copy; 2015 <a href="http://rotterdam.luminis.eu/" target="_blank">Luminis Rotterdam</a>. All Rights Reserved. Designed by <a href="http://8guild.com/" target="_blank">8Guild</a></p>
                            </div>
                            <div className="col-lg-5 col-md-5 col-sm-5">
                                <div className="payment">
                                    <img src="/img/payment/visa.png" alt="Visa"/>
                                    <img src="/img/payment/paypal.png" alt="PayPal"/>
                                    <img src="/img/payment/master.png" alt="Master Card"/>
                                    <img src="/img/payment/discover.png" alt="Discover"/>
                                    <img src="/img/payment/amazon.png" alt="Amazon"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
});

module.exports = Footer;
