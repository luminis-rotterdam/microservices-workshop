var React = require('react');
var Link = require('react-router').Link;
var accounting = require('accounting');

var CartActions = require('../actions/CartActions');
var CartStore = require('../stores/CartStore');
var CartPersistence = require('../utils/CartPersistence');

/**
 * Retrieve the current CARTITMES from the CartStore
 */
function getCartState() {
    return {
        cartItems: CartStore.getAll(),
        cartTotalCost: accounting.formatMoney(CartStore.getTotalCost()),
        cartTotalItems: CartStore.getTotalItems(),
    };
}

var Cart = React.createClass({

    onDelete: function (id) {
        CartActions.remove(id);
    },

    onUpdate: function (id, event) {
        CartActions.update(id, Number(event.target.value));
    },

    getInitialState: function () {
        CartActions.loadCart();
        return getCartState();
    },

    componentDidMount: function () {
        CartStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        CartStore.removeChangeListener(this._onChange);
    },

    render: function () {
        return (
            <div className="cart-btn">
                <Link className="btn btn-outlined-invert" to="shopping-cart"><i
                    className="icon-shopping-cart-content"></i><span>{this.state.cartTotalItems}</span><b>{this.state.cartTotalCost + ' $'}</b></Link>

                <div className="cart-dropdown">
                    <span></span>
                    <div className="body">
                        <table>
                            <thead>
                            <tr>
                                <th>Items</th>
                                <th>Quantity</th>
                                <th>Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.cartItems.map(function (item) {
                                    return (
                                        <tr key={item.id} className="item">
                                            <td>
                                                <div onClick={this.onDelete.bind(this, item.id)}
                                                     className="delete"></div>
                                                <Link to="productDetail" params={{category: item.category, id: item.id}}
                                                      href="#">{item.name}</Link></td>
                                            <td><input onChange={this.onUpdate.bind(this, item.id)} type="number"
                                                       value={item.amount} min="1"/></td>
                                            <td className="price">{accounting.formatNumber(item.cost, 2, ".", ",")}</td>
                                        </tr>
                                    )
                                }.bind(this))
                            }
                            </tbody>
                        </table>
                    </div>
                    <div className="footer group">
                        <div className="buttons">
                            <Link className="btn btn-outlined-invert" to="/checkout"><i className="icon-download"></i>Checkout</Link>
                            <Link className="btn btn-outlined-invert" to="/shopping-cart"><i
                                className="icon-shopping-cart-content"></i>To cart</Link>
                        </div>
                        <div className="total">{this.state.cartTotalCost}</div>
                    </div>
                </div>
            </div>
        );
    },

    /**
     * Event handler for 'change' events coming from the CartStore
     */
    _onChange: function () {
        CartPersistence.storeCart(CartStore.getAll());
        this.setState(getCartState());
    }

});

module.exports = Cart;