var React = require('react');

var LoginActions = require('../actions/LoginActions');
var LoginStore = require('../stores/LoginStore');
var LoginConstants = require('../constants/StoreConstants');

function getLoginState() {
    return {
        userName: LoginStore.getUserName(),
        loginState: LoginStore.getState()
    }
}

function firstLetterOfUserName() {
    return getLoginState().userName.charAt(0);
}

function restOfUserName() {
    return getLoginState().userName.substring(1);
}

var Login = React.createClass({
    onLogin: function (event) {
        event.preventDefault();
        LoginActions.login(document.getElementById("log-email").value, document.getElementById("log-password").value);
    },

    onLogout: function (event) {
        event.preventDefault();
        LoginActions.logout();
    },

    _onChange: function () {
        this.setState(getLoginState());
    },

    getInitialState: function () {
        LoginActions.loadFromSession();
        return getLoginState();
    },

    componentDidMount: function () {
        LoginStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        LoginStore.removeChangeListener(this._onChange);
    },

    render: function () {
        return (
            <span>
                {
                    function() {
                        if (this.state.loginState == LoginConstants.LOGGED_IN) {
                            $("#loginModal").modal("hide");
                            return (
                                <a className="login-btn btn-outlined-invert" onClick={this.onLogout}><i className="icon-profile-remove"></i><span><b>{firstLetterOfUserName()}</b>{restOfUserName()}</span></a>
                            );
                        } else {
                            return (
                                <a className="login-btn btn-outlined-invert" href="#" data-toggle="modal"
                                   data-target="#loginModal"><i className="icon-profile"></i><span><b>L</b>ogin</span></a>
                            );
                        }
                    }.bind(this)()
                }

                <div className="modal fade" id="loginModal" tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><i
                                    className="fa fa-times"></i></button>
                                <h2>Login</h2>
                            </div>
                            {
                                function() {
                                    if (this.state.loginState == LoginConstants.LOGIN_FAILED) {
                                        return (
                                            <div className="alert alert-warning alert-dismissible fade in" role="alert">
                                                <strong>Could not log you in!</strong> Please try again, and hope for the best.
                                            </div>
                                        );
                                    }
                                }.bind(this)()
                            }
                            <div className="modal-body">
                                <form className="login-form" onSubmit={this.onLogin}>
                                    <div className="form-group group">
                                        <label htmlFor="log-email">Email</label>
                                        <input type="email" className="form-control" name="log-email" id="log-email"
                                               placeholder="Enter your email" required="required" autoFocus="true" />
                                    </div>

                                    <div className="form-group group">
                                        <label htmlFor="log-password">Password</label>
                                        <input type="password" className="form-control" name="log-password"
                                               id="log-password" placeholder="Enter your password" required="required"/>
                                    </div>
                                    <input className="btn btn-black" type="submit" value="Login"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
        );
    }
});

module.exports = Login;
