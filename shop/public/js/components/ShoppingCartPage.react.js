var React = require('react');
var Link = require('react-router').Link;

var ShoppingCart = require('./ShoppingCart.react');
var Suggestions = require('./Suggestions.react');

var ShoppingCartPage = React.createClass({
    render: function () {
        return (
        <div className="page-content">

            <ol className="breadcrumb">
                <li><Link to="/">Home</Link></li>
                <li>Shopping cart</li>
            </ol>
            <ShoppingCart />

            <Suggestions quantity="4" title="You may also like" />

        </div>
        );
    }
});

module.exports = ShoppingCartPage;