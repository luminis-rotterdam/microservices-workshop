var React = require('react');
var Cart = require('./Cart.react');
var Login = require('./Login.react');
var Search = require('./Search.react');

var Toolbar = React.createClass({

    render: function () {
        return (
            <div className="toolbar-container">
                <div className="container">
                    <div className="toolbar group">
                        <Login />
                        <Cart />
                        <button className="search-btn btn-outlined-invert"><i className="icon-magnifier"></i></button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Toolbar;