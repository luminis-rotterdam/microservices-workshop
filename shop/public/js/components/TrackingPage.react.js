var React = require('react');
var Link = require('react-router').Link;
var _ = require('lodash');

var OrderActions = require('../actions/OrderActions');
var OrderStore = require('../stores/OrderStore');

var LoadingComponent = require('./LoadingComponent.react');

function getDeliveryState(orderNumber, originalState) {
    var state = {
        order: OrderStore.findBy(orderNumber),
        loading: OrderStore.isLoadingOrderHistory()
    };
    if (originalState.newOrderNumber) {
        state.newOrderNumber = originalState.newOrderNumber;
    }
    return state;
}

var TrackingPage = React.createClass({

    componentDidMount: function () {
        OrderStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        OrderStore.removeChangeListener(this._onChange);
    },

    getInitialState: function () {
        var state = {
            loading: true
        };
        if (OrderStore.hasNewOrder()) {
            state.newOrderNumber = OrderStore.getLastOrderNumner();
        }
        setTimeout(OrderActions.loadOrders, 0)
        return state;
    },

    _onChange: function () {
        this.setState(getDeliveryState(this.props.params.id, this.state));
    },

    render: function () {
        return (
            <div className="page-content">
                {
                    function () {
                        if (this.state.newOrderNumber) {
                            return (
                                <div className="alert alert-success alert-dismissible fade in" role="alert">
                                    <strong>New order created with number: {this.state.newOrderNumber}</strong>.
                                </div>
                            );
                        }
                    }.bind(this)()
                }

                <ol className="breadcrumb">
                    <li><Link to="/">Home</Link></li>
                    <li>Delivery</li>
                </ol>
                <section className="tracking">
                    <LoadingComponent {...this.state} element={TrackingDetails}/>
                </section>
            </div>
        );
    }

});

function isDoneClassName(check, state) {
    var stateOrder = ['preparation', 'shipped', 'inTransit', 'delivery', 'delivered'];
    var checkIndex = _.indexOf(stateOrder, check);
    var stateIndex = _.indexOf(stateOrder, state);
    if (checkIndex <= stateIndex) {
        return 'done';
    } else {
        return '';
    }
}

var TrackingDetails = React.createClass({

    render: function () {
        var order = this.props.order;
        return (
            <div className="container">
                <div className="row">
                    <h2 className="title">Track Your Package</h2>

                    <div className="row space-top">
                        <div className="clo-lg-8 col-md-8 col-sm-8 space-bottom">
                            <h4 className="light-weight uppercase">Shipment Tracking</h4>

                            <div className="ship-scale">

                                      <span className={'round ' + isDoneClassName('preparation', order.shippingState)}>
                                        <i className="fa fa-check"></i>

                                        <span className="textik">Shipping soon</span>
                                        <span className="textik2">10%</span>

                                      </span>

                                      <span className={'flat ' + isDoneClassName('shipped', order.shippingState)}>
                                        <span className="round">
                                          <i className="fa fa-check"></i>

                                          <span className="textik">Shipped</span>
                                          <span className="textik2">25%</span>

                                        </span>
                                      </span>

                                      <span className={'flat ' + isDoneClassName('inTransit', order.shippingState)}>
                                        <span className="round">
                                          <i className="fa fa-check"></i>

                                          <span className="textik">In transit</span>
                                          <span className="textik2">50%</span>

                                        </span>
                                      </span>

                                      <span className={'flat ' + isDoneClassName('delivery', order.shippingState)}>
                                        <span className="round">
                                          <i className="fa fa-check"></i>

                                          <span className="textik">Out for delivery</span>
                                          <span className="textik2">75%</span>

                                        </span>
                                      </span>

                                      <span className={'flat ' + isDoneClassName('delivered', order.shippingState)}>
                                        <span className="round">
                                          <i className="fa fa-check"></i>

                                          <span className="textik">Delivered</span>
                                          <span className="textik2">100%</span>

                                        </span>
                                      </span>
                            </div>
                        </div>
                        <div className="clo-lg-4 col-md-4 col-sm-4 space-bottom">
                            <h4 className="light-weight uppercase">Shipment information</h4>

                            <div className="shipment">
                                <div className="shipment-title">Delivering to</div>

                                <div>
                                    {order.contactInformation.name} <br />
                                    {order.shippingAddress.street} <br />
                                    {order.shippingAddress.city + ', ' + order.shippingAddress.postCode} <br />
                                    {order.contactInformation.phone} <br />
                                </div>
                            </div>

                            <div className="shipment">
                                <div className="shipment-title">Carrier</div>

                                <div>
                                    {order.carrierInformation.carrier}
                                </div>
                            </div>

                            <div className="shipment">
                                <div className="shipment-title">Tracking #</div>

                                <div>
                                    {order.carrierInformation.trackingId}
                                </div>
                            </div>

                            <div className="shipment">
                                <div className="shipment-title">Order #</div>

                                <div>
                                    {order.orderNumber}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        );
    }
});

module.exports = TrackingPage;