var React = require('react');
var Link = require('react-router').Link;
var Search = require('./Search.react');

var Toolbar = require('./Toolbar.react.js');

var Header = React.createClass({
    render: function() {
        return (
            <header data-offset-top="500" data-stuck="600">

                <Search />

                <div className="menu-toggle"><i className="fa fa-list"></i></div>

                    <div className="container">
                        <a className="logo" href="/"><img src="/custom-images/new-logo.png" alt="Liquid Sunshine"/></a>
                    </div>

                    <nav className="menu">
                        <div className="container">

                            <ul className="main">
                                <li><Link to="/"><b>H</b>ome</Link></li>
                                <li className="has-submenu"><Link to="/products"><b>S</b>hop<i className="fa fa-chevron-down"></i></Link>
                                    <ul className="submenu">
                                        <li><Link to="/products/blends">Blends</Link></li>
                                        <li><Link to="/products/single-malt">Single Malt</Link></li>
                                        <li><Link to="/products/irish">Irish</Link></li>
                                        <li><Link to="/products/bourbon">Bourbon</Link></li>
                                        <li><Link to="/products/international">International</Link></li>
                                        <li><Link to="/products/exclusive">Exclusive</Link></li>
                                    </ul>
                                </li>
                                <li><Link to="/shopping-cart"><b>S</b>hopping Cart</Link></li>
                                <li><Link to="/checkout"><b>C</b>heckout</Link></li>
                                <li><Link to="/history"><b>H</b>istory</Link></li>
                            </ul>

                        </div>

                        <div className="catalog-block">
                            <div className="container">
                                <ul className="catalog">
                                    <li><Link to="/products/blends">Blends</Link></li>
                                    <li><Link to="/products/single-malt">Single Malt</Link></li>
                                    <li><Link to="/products/irish">Irish</Link></li>
                                    <li><Link to="/products/bourbon">Bourbon</Link></li>
                                    <li><Link to="/products/international">International</Link></li>
                                    <li><Link to="/products/exclusive">Exclusive</Link></li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <Toolbar />
                </header>
        );
    }
});

module.exports = Header;