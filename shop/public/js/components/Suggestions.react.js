var React = require('react');
var RecommendationActions = require('../actions/RecommendationActions');
var RecommendationStore = require('../stores/RecommendationStore');

var ProductTile = require('./ProductTile.react');

/**
 * Retrieve the current Suggestions from the ProductStore
 */
function getProductSuggestions() {
    return {
        suggestions: RecommendationStore.getReccomendations(),
        isLoading: RecommendationStore.isLoading()
    }
}

var Suggestions = React.createClass({

    getInitialState: function () {
        return { isLoading: true}
    },

    componentDidMount: function () {
        RecommendationActions.loadRecommendations(this.props.quantity, this.props.productIds);
        RecommendationStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        RecommendationStore.removeChangeListener(this._onChange);
    },

    render: function () {
        if (this.state.isLoading) {
            return (
                <section className="catalog-grid">
                    <div className="container">
                        <h2 className="spinner"><i className="fa fa-spinner fa-spin"></i></h2>
                    </div>
                </section>
            );
        } else {
            return (
                <section className="catalog-grid">
                    <div className="container">
                        <h2 className="dark-color">{this.props.title}</h2>

                        <div className="row">
                            {
                                this.state.suggestions.map(function (product) {
                                    return <ProductTile className="col-lg-3 col-md-4 col-sm-6" key={product.id} product={product}/>
                                })
                            }
                        </div>
                    </div>
                </section>
            );
        }
    },

    /**
     * Event handler for 'change' events coming from the ProductStore
     */
    _onChange: function () {
        this.setState(getProductSuggestions(this.props.quantity));
    }

});


module.exports = Suggestions;

