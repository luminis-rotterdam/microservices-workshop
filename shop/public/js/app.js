var React = require('react');
var ReactDom = require('react-dom');

var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;

var Header = require('./components/Header.react.js');
var Footer = require('./components/Footer.react.js');
var LandingPage = require('./components/LandingPage.react');
var ShoppingCartPage = require('./components/ShoppingCartPage.react');
var ProductDetailPage = require('./components/ProductDetailPage.react');
var ProductListPage = require('./components/ProductListPage.react');
var CheckoutPage = require('./components/CheckoutPage.react');
var OrderHistoryPage = require('./components/OrderHistoryPage.react');
var TrackingPage = require('./components/TrackingPage.react');

var accounting = require('accounting');
accounting.settings.currency.format = "%v %s";
accounting.settings.currency.decimal = ',';
accounting.settings.currency.thousand = '.';
accounting.settings.currency.symbol = '€';

var App = React.createClass({
    render: function () {
        return (
            <div>
                <Header />
                {this.props.children}
                <Footer />
            </div>
        );
    }
});

ReactDom.render((
    <Router history={ReactRouter.browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={LandingPage}/>
            <Route path="shopping-cart" component={ShoppingCartPage}/>
            <Route path="checkout" component={CheckoutPage}/>
            <Route path="history" component={OrderHistoryPage}/>
            <Route path="tracking/:id" component={TrackingPage}/>
            <Route path="products" component={ProductListPage}/>
            <Route path="products/:category" component={ProductListPage}/>
            <Route path="products/:category/:id" component={ProductDetailPage}/>
        </Route>
    </Router>
), document.getElementById("main"));