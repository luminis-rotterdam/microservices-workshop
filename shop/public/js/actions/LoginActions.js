var AppDispatcher = require('../dispatcher/AppDispatcher');
var LoginConstants = require('../constants/StoreConstants');
var LoginPersistence = require('../utils/LoginPersistence');

var LoginActions = {

    loadFromSession: function() {
        var userData = LoginPersistence.loadUserData();
        if (userData) {
            AppDispatcher.dispatch({
                actionType: LoginConstants.LOGGED_IN,
                data: userData
            });
        }
    },

    login: function (username, password) {
        AppDispatcher.dispatch({
            actionType: LoginConstants.LOGGING_IN,
        });
        doLogin(username, password);
    },

    logout: function () {
        AppDispatcher.dispatch({
            actionType: LoginConstants.LOGGED_OUT,
        });
        LoginPersistence.logout();
    }

};

function doLogin(username, password) {
    $.ajax({
        url: '/api/login',
        data: { username: username, password: password },
        method: 'post',
        success: function (data) {
            AppDispatcher.dispatch({
                actionType: LoginConstants.LOGGED_IN,
                data: data
            });
            LoginPersistence.storeUserData(data);
        },
        error: function (xhr, status, err) {
            AppDispatcher.dispatch({
                actionType: LoginConstants.LOGIN_FAILED
            });
            LoginPersistence.logout();
        }
    });
}

module.exports = LoginActions;
