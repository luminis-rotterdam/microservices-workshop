var AppDispatcher = require('../dispatcher/AppDispatcher');
var CartConstants = require('../constants/StoreConstants');
var CartPersistence = require('../utils/CartPersistence');

var CartActions = {

    loadCart: function () {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_LOADED,
            data: CartPersistence.loadCart()
        });
    },

    add: function (product) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_ADD,
            product: product
        });
    },

    update: function (id, amount) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_UPDATE,
            id: id,
            amount: amount
        });
    },

    remove: function (id) {
        AppDispatcher.dispatch({
            actionType: CartConstants.CART_REMOVE,
            id: id
        });
    },

    clear: function() {
        AppDispatcher.dispatch({
           actionType: CartConstants.CART_CLEAR
        });
    }

};

module.exports = CartActions;
