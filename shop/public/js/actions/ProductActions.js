var AppDispatcher = require('../dispatcher/AppDispatcher');
var ProductsConstants = require('../constants/StoreConstants');

var CartActions = {

    loadProducts: function (query) {
        AppDispatcher.dispatch({
            actionType: ProductsConstants.GET_PRODUCTS,
        });
        doLoadProducts(query);
    }

};

function doLoadProducts(query) {
    var url = '/api/products';
    if (query) {
        url += '/search?q=' + query;
    }
    $.ajax({
        url: url,
        dataType: 'json',
        cache: false,
        success: function (data, status, request) {
            AppDispatcher.dispatch({
                actionType: ProductsConstants.PRODUCTS_LOADED,
                data: data,
                development: request.getResponseHeader('Development-Mode')
            });
        },
        error: function (xhr, status, err) {
            console.error(url, status, err.toString());
        }
    });
}

module.exports = CartActions;
