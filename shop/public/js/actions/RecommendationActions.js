var AppDispatcher = require('../dispatcher/AppDispatcher');
var StoreConstants = require('../constants/StoreConstants');
var LoginStore = require('../stores/LoginStore');

var RecommendationActions = {

    loadRecommendations: function (quantity, products) {
        AppDispatcher.dispatch({
            actionType: StoreConstants.RECOMMENDATIONS_LOADING,
        });
        doLoadRecommendations(quantity, products);
    },

};

function doLoadRecommendations(quantity, products) {
    var url = '/api/recommendations?quantity=' + quantity;
    if (products) {
        url += '&products=' + products.join(",")
    }
    if (LoginStore.isLoggedIn()) {
        url += '&user=' + LoginStore.getUserName();
    }
    $.ajax({
        url: url,
        dataType: 'json',
        cache: false,
        success: function (data) {
            AppDispatcher.dispatch({
                actionType: StoreConstants.RECOMMENDATIONS_LOADED,
                data: data
            });
        },
        error: function (xhr, status, err) {
            console.error(url, status, err.toString());
            AppDispatcher.dispatch({
                actionType: StoreConstants.RECOMMENDATIONS_LOADED,
                data: []
            });
        }
    });
}

module.exports = RecommendationActions;
