var AppDispatcher = require('../dispatcher/AppDispatcher');
var OrderConstants = require('../constants/StoreConstants');
var LoginStore = require('../stores/LoginStore');
var CartStore = require('../stores/CartStore');
var CartActions = require('../actions/CartActions');

var OrderActions = {

    placeOrder: function () {
        AppDispatcher.dispatch({
            actionType: OrderConstants.ORDER_CREATING,
        });
        doPlaceOrder();
    },

    loadOrders: function () {
        AppDispatcher.dispatch({
            actionType: OrderConstants.ORDERS_LOADING,
        });
        doLoadOrders();
    }

};

function doPlaceOrder() {
    $.ajax({
        url: '/api/orders',
        data: JSON.stringify(buildOrderData()),
        method: 'post',
        success: function (data) {
            AppDispatcher.dispatch({
                actionType: OrderConstants.ORDER_CREATED,
                data: data
            });
            CartActions.clear();
        },
        error: function () {
            AppDispatcher.dispatch({
                actionType: OrderConstants.ORDER_FAILED
            });
        }
    });
}

function buildOrderData() {
    return {
        user: LoginStore.getUserName(),
        products: CartStore.getAll().map(function (item) {
            return {
                id: item.id,
                quantity: item.amount
            }
        })
    };
}

function doLoadOrders() {
    var url = '/api/orders?user=' + LoginStore.getUserName();
    $.ajax({
        url: url,
        dataType: 'json',
        cache: false,
        success: function (data) {
            AppDispatcher.dispatch({
                actionType: OrderConstants.ORDERS_LOADED,
                data: data
            });
        },
        error: function (xhr, status, err) {
            console.error(url, status, err.toString());
        }
    });
}

module.exports = OrderActions;
