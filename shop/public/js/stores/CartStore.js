var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var CartConstants = require('../constants/StoreConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _cartItems = {};

function initCart(items) {
    items.map(function(item) {
        _cartItems[item.id] = item;
    });
}

/**
 * Add a CART item.
 * @param  {id} The ID of the product
 */
function add(product) {
    var id = product.id;
    if (_cartItems[id]) {
        update(id, _cartItems[id].amount + 1)
    } else {
        _cartItems[id] = {
            id: id,
            amount: 1,
            name: product.name,
            category: product.category,
            productCost: product.cost,
            cost: product.cost
        };
    }
}

function update(id, amount) {
    if (amount > 0) {
        _cartItems[id].amount = amount;
        _cartItems[id].cost = _cartItems[id].amount * _cartItems[id].productCost;
    }
}

function remove(id) {
    delete _cartItems[id];
}

var CartStore = assign({}, EventEmitter.prototype, {

    /**
     * Get the entire collection of TODOs.
     * @return {object}
     */
    getAll: function () {
        var items = [];

        for (var key in _cartItems) {
            items.push(_cartItems[key]);
        }

        return items;
    },

    getTotalCost: function () {
        return this.getAll().reduce(function(previous, item) {
            return previous + item.cost;
        }, 0);
    },

    getTotalItems: function () {
        return this.getAll().reduce(function(previous, item) {
            return previous + item.amount;
        }, 0);
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register callback to handle all updates
AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case CartConstants.CART_LOADED:
            initCart(action.data);
            break;
        case CartConstants.CART_ADD:
            add(action.product);
            break;
        case CartConstants.CART_UPDATE:
            update(action.id, action.amount);
            break;
        case CartConstants.CART_REMOVE:
            remove(action.id);
            break;
        case CartConstants.CART_CLEAR:
            _cartItems = {};
            break;
        default:
            return;
    }
    CartStore.emitChange();
});

module.exports = CartStore;
