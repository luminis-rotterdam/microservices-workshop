var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var OrderConstants = require('../constants/StoreConstants');
var assign = require('object-assign');
var _ = require('lodash');

var CHANGE_EVENT = 'change';

var _lastOrder = null;
var _orderFailed = false;
var _orderingInProgress = false;
var _ordersLoading = false;
var _orderHistory = [];

var OrderStore = assign({}, EventEmitter.prototype, {

    isOrderFailed: function() {
        return _orderFailed;
    },

    isOrderInProgress: function() {
        return _orderingInProgress;
    },

    hasNewOrder: function() {
        return _lastOrder != null;
    },

    getLastOrderNumner: function() {
        return _lastOrder;
    },

    isLoadingOrderHistory: function() {
        return _ordersLoading
    },

    getOrderHistory: function() {
        return _orderHistory;
    },

    findBy: function(orderNumber) {
        return _.find(_orderHistory, ['orderNumber', parseInt(orderNumber)]);
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (action) {
    _orderingInProgress = false;
    _orderFailed = false;
    _lastOrder = null;
    _ordersLoading = false;
    switch (action.actionType) {
        case OrderConstants.ORDER_CREATING:
            _orderingInProgress= true;
            break;
        case OrderConstants.ORDER_FAILED:
            _orderFailed = true;
            break;
        case OrderConstants.ORDER_CREATED:
            _lastOrder = action.data.orderNumber;
            break;
        case OrderConstants.ORDERS_LOADING:
            _ordersLoading = true;
            break;
        case OrderConstants.ORDERS_LOADED:
            _orderHistory = action.data;
            break;
        default:
            return;
    }
    OrderStore.emitChange();
});

module.exports = OrderStore;
