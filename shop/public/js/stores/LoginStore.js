var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var LoginConstants = require('../constants/StoreConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _user = {
    userName: null,
    userInfo: {}
};
var _state = LoginConstants.LOGGED_OUT;

var LoginStore = assign({}, EventEmitter.prototype, {

    getState: function() {
        return _state;
    },

    getUserName: function() {
        return _user.userName;
    },

    getUserInfo: function() {
        return _user.userInfo;
    },

    isLoggedIn: function() {
        return (_state == LoginConstants.LOGGED_IN);
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register callback to handle all updates
AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case LoginConstants.LOGGED_OUT:
        case LoginConstants.LOGGING_IN:
        case LoginConstants.LOGIN_FAILED:
            _state = action.actionType;
            _user = {
                userName: null,
                userInfo: {}
            };
            break;
        case LoginConstants.LOGGED_IN:
            _state = action.actionType;
            _user = action.data;
            break;
        default:
            return;
    }
    LoginStore.emitChange();
});

module.exports = LoginStore;
