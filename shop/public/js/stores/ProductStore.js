var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var ProductConstants = require('../constants/StoreConstants');
var assign = require('object-assign');
var _ = require('lodash');

var CHANGE_EVENT = 'change';

var _products = [];
var loading = false;
var development = false;

function setLoading() {
    loading = true;
}

function addAll(products) {
    loading = false;
    _products = products;
}

var ProductStore = assign({}, EventEmitter.prototype, {

    getSuggestions: function(quantity) {
        var size = _products.length < quantity ? _products.length : quantity;
        return _.shuffle(_products).slice(0, size);
    },

    getProducts: function() {
        return _products;
    },

    isDevelopment: function() {
        return development;
    },

    findBy: function(id) {
        return _.find(_products, 'id', id);
    },

    isLoading: function() {
        return loading;
    },

    getProductsForCategory: function(category) {
        return _products.reduce(function(result, current) {
            if (current.category == category) {
                result.push(current);
            }
            return result;
        }, []);
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    /**
     * @param {function} callback
     */
    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

// Register callback to handle all updates
ProductStore.dispatchToken = AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case ProductConstants.GET_PRODUCTS:
            setLoading();
            break;
        case ProductConstants.PRODUCTS_LOADED:
            addAll(action.data);
            development = action.development || false;
            break;
        default:
            return;
    }
    ProductStore.emitChange();
});

module.exports = ProductStore;
