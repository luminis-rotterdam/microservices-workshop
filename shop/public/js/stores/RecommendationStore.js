var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var StoreConstants = require('../constants/StoreConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _recommendations = [];
var _loading = false;

function setLoading() {
    _loading = true;
}

function addAll(products) {
    _loading = false;
    _recommendations = products;
}

var RecommendationStore = assign({}, EventEmitter.prototype, {

    getReccomendations: function() {
        return _recommendations;
    },

    isLoading: function() {
        return _loading;
    },

    emitChange: function () {
        this.emit(CHANGE_EVENT);
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

AppDispatcher.register(function (action) {
    switch (action.actionType) {
        case StoreConstants.RECOMMENDATIONS_LOADING:
            setLoading();
            break;
        case StoreConstants.RECOMMENDATIONS_LOADED:
            addAll(action.data);
            break;
        default:
            return;
    }
    RecommendationStore.emitChange();
});

module.exports = RecommendationStore;
