"use strict";
var app = require('express')(),
    config = require('./config.json'),
    consul = require('consul')(config.consul || {}),
    request = require('request'),
    _ = require('lodash'),
    SDC = require('statsd-client'),
    serviceRegistry = {};

bootstrap();

app.get('/api/products', function (req, res) {

    function callProductService(endpoint) {
        request
            .get(endpoint + '/products')
            .on('error', fallback)
            .pipe(res);
    };

    function fallback() {
        res.json([]);
    };

    function fakeProductService() {
        res.set('Development-Mode', 'true');
        res.sendFile(__dirname + "/data/products.json");
    };

    callService({
        id: 'products',
        command: callProductService,
        fallback: fallback,
        development: fakeProductService
    });

});

app.get('/api/products/search', function (req, res) {
    var query = req.query.q;
    callService({
        id: 'search',
        command: function (endpoint) {
            request.get(endpoint + '/search?q=' + q).pipe(res);
        },
        development: function () {
            var products = require(__dirname + "/data/products.json");
            var result = _.filter(products, function (product) {
                var text = _.values(product).join('').toLowerCase();
                return _.includes(text, query.toLowerCase());
            });
            res.json(result);
        },
        fallback: function() {
            res.json([]);
        }
    });
});

app.get('/api/orders', function (req, res) {
    var user = req.query.user;
    callService({
        id: 'orders',
        command: function (endpoint) {
            request.get(endpoint + '/orders?user=' + user).pipe(res);
        },
        development: function () {
            res.sendFile(__dirname + "/data/orders.json");
        }
    });
});

app.post('/api/orders', function (req, res) {
    callService({
        id: 'orders',
        command: function (endpoint) {
            req.pipe(request.post(endpoint + '/orders').pipe(res));
        },
        development: function () {
            res.json({
                orderNumber: 42
            })
        }
    });
});

app.post('/api/login', function (req, res) {
    var username = req.body.username;
    var password = req.body.password
    callService({
        id: 'authentication',
        command: function (endpoint) {
            request.post({
                url: endpoint +'/login',
                body: {
                    username: username,
                    password: password
                },
                json: true
            }).pipe(res);
        },
        development: function () {
            if (username === "fault@gmail.com") {
                res.sendStatus(403)
            } else {
                res.json({
                    userName: 'Willem Dekker',
                    userInfo: {
                        firstName: "Willem",
                        lastName: "Dekker",
                        street: "Kasteelweg 51",
                        city: "Rotterdam",
                        state: "Zuid-Holland",
                        postCode: "3077 BN",
                        email: "willem.dekker@luminis.eu",
                        phone: "+31 88 58 64 640",
                        country: "Nederland"
                    }
                });
            }
        }
    });
});

app.get('/api/recommendations', function (req, res) {
    var quantity = req.body.quantity || 4;
    callService({
        id: 'recommendations',
        command: function (endpoint) {
            request.get(endpoint + '/recommendations?quantity=' + quantity).pipe(res);
        },
        development: function () {
            var products = require(__dirname + "/data/products.json");
            var size = products.length < quantity ? products.length : quantity;
            var suggestions = _.shuffle(products).slice(0, size);
            res.json(suggestions);
        },
        fallback: function() {
            res.json([]);
        }
    });
});

app.get('*', function (req, res) {
    res.sendFile(__dirname + "/public/index.html");
});

function callService(args) {
    var serviceId = args.id,
        service = findRandomService(serviceId),
        command = args.command,
        development = args.development,
        fallback = args.fallback;

    if (service && command) {
        console.log("Got service endpoint:", service.serviceEndpoint, 'for service id:', serviceId);
        logServiceCall(serviceId);
        return setImmediate(command.bind(this, service.serviceEndpoint));
    } else if (config.services[serviceId].development && development) {
        console.log("Faking service:", serviceId, " because it is development-mode");
        logServiceCall(serviceId, "fake");
        return setTimeout(development, Math.random() * 3000);
    } else if (fallback) {
        console.warn("No service found for:", serviceId, "falling back");
        logServiceCall(serviceId, "fallback");
        return setImmediate(fallback);
    }
    throw new Error("No service or fallback available for service: " + serviceId + ". Please provide one.");

    function findRandomService(serviceId) {
        var servicesAvailable = serviceRegistry[serviceId];
        if (servicesAvailable) {
            return servicesAvailable[Math.floor(Math.random() * servicesAvailable.length)];
        } else {
            return null;
        }
    }

    function logServiceCall(id, prefix) {
        var service = findRandomService('metrics');
        if (service) {
            var sdc = new SDC({host: service.address, port: service.port});
            console.log('Calling metrics for service:', id);
            if (prefix) {
                prefix = "shop." + prefix + ".";
            } else {
                prefix = "shop.";
            }
            sdc.increment(prefix + id);
            sdc.close();
        } else {
            console.log("Cannot find 'metrics' service, nothing logged.")
        }
    }

}

function bootstrap() {
    initServer()
    startServer();
    startListeningForConsulServices();
}

function initServer() {
    var compress = require('compression'),
        express = require('express'),
        bodyParser = require('body-parser');
    request = request.defaults({timeout: 4000});
    app.use(compress());
    app.use(express.static('public'));
    app.use(bodyParser.urlencoded({extended: true}));
}

function startServer() {
    var port = config.port || 3000;
    console.log('liquid-sunshine server listening on port:', port);
    app.listen(port);
}

function startListeningForConsulServices() {
    for (var key in config.services) {
        listenForService(key);
    }
}

function listenForService(serviceId) {
    if (config.services[serviceId].development) {
        console.log("Service", serviceId, "is in development mode.");
    } else {
        console.log("Service", serviceId, "is in production mode. Start listening for services");
        createWatch(serviceId)
    }
}

function createWatch(serviceId) {
    var watch = consul.watch({method: consul.health.service, options: {service: serviceId, passing: true}});

    watch.on('change', function (data) {
        var old = JSON.stringify(serviceRegistry);
        serviceRegistry[serviceId] = [];
        data.map(function (serviceInfo) {
            var address = serviceInfo.Service.Address || serviceInfo.Node.Address;
            var port = serviceInfo.Service.Port || 80;
            var serviceEndpoint = 'http://' + address + ':' + port;
            serviceRegistry[serviceId].push({
                serviceEndpoint: serviceEndpoint,
                address: address,
                port: port
            });
        });
        if (old !== JSON.stringify(serviceRegistry)) {
            console.log('Something changed for', serviceId)
            console.log(serviceRegistry);
        }
    });

    watch.on('error', function (err) {
        console.error("Error talking to consul for service: " + serviceId + ", did you start the agent?", err);
        process.exit();
    });
}
