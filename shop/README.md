# Liquid Sunshine Webshop quickstart
A simple webserver for the liquid-sunshine webshop

## To run
1. install node.js (including NPM): https://nodejs.org/ (build and tested with version 4.0)
2. run start.sh to build and run
3. serve to http://localhost:3000

## Configuration
Modify the `config.json` file to change service from `development` mode to `production`:

### Example

Product service `development` mode:

		"products": {
          "development": true
        },

to `production` mode:

		"products": {
          "development": false
        },

**NOTE:** You can also change the listen port in the configuration file.