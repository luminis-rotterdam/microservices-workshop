#!/bin/sh

ulimit -n 2560

if which npm >/dev/null; then
    npm prune
    npm install
    npm start
else
    echo "npm not found, please install node and npm: https://nodejs.org/"
fi
