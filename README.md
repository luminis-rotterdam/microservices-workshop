# Liquid Sunshine Workshop
The web-shop, backoffice care-center and a consul demo.
Check [microservices architecture.pdf](microservices%20architecture.pdf) for more details on the shops architecture.

## To run
1. install node.js (including NPM): https://nodejs.org/ (build and tested with version 6.1.0)
2. configure the join ip-address in run-agent to the consul cluster (We let you know what it is). 
3. run start.sh to start a consul-agent, build and run the webshop and care-center.
4. surf to http://localhost:3000 for the shop and http://localhost:4000 for the care-center.
5. change the config.json in care-center or the webshop to switch from development mode to you actual service.

## For development
Run start-dev.sh to start the same processes but with a consul server running locally and a consul UI on http://localhost:8500/ui

## Tips
* If you have troubles restarting consul try to remove the data folder: "/tmp/consul"

## note for mac users
You might need to install browserify and consul for mac first
(only if you want to run consul on the mac and not in vagrant)
1. npm install -g browserify
2. download and extract : https://releases.hashicorp.com/consul/0.6.4/consul_0.6.4_darwin_amd64.zip